


def work_on_folder():
    f1 = open('d:/itay/hello.txt')
    for line in f1:
        print(line, end="")
    x = int(input("\nEnter number:")) # NaN  "0"

try:
    work_on_folder()
except OSError as err: # catch
    print(f"---- file error: {err}")

#f1 = open('d:/itay/hello.txt')
f1 = None
try:
    f1 = open('d:/itay/hello.txt')
    for line in f1:
        print(line, end="")
    x = int(input("\nEnter number:")) # NaN  "0"
except OSError as err: # catch
    print(f"---- file error: {err}")

class NotANumber(Exception):
    def __init__(self, exp):
        self.message = "This is not a number: " + exp
        Exception.__init__(self, self.message)
class WrongATMPassCode(Exception):
    def __init__(self, code):
        self.message = "ATM wrong code: "+ code
        Exception.__init__(self, self.message)
try:
    #
    raise WrongATMPassCode('1254')

except WrongATMPassCode as err:
    print(f'---- Not the code -- {err}')

try:
    raise NotANumber("asdasd")
    raise OSError("file not found")
    #
    #
except NotANumber as err:
    print(f'---- Not a number error {err}')

def my_div():
    while True:
        try:
            x = int(input("First number:"))
            y = int(input("Second number:"))
            z = x / y
            return z
        except ValueError as err:
            print(f'---- input format error {err}')
        except ZeroDivisionError as err:
            print(f'---- divide by zero {err}')

my_div()

import os
filename = input("Enter file name: ")

try:
    f = open(filename, "r")

    for line in f:
        print(line, end="")

except FileNotFoundError:
    print("File not found")

except PermissionError:
    print("You don't have the permission to read the file")

except FileExistsError:
    print("You don't have the permission to read the file")



else:
    print("\nProgram ran without any problem")

finally:
    if f != None:
        f.close()
    print("finally clause: This will always execute")

'''
with open('c:/itay/hello.txt') as f1:
    for line in f1:
        print(line, end="")
    f1.seek(0)
    # because of the with --> here the open will be closed
    # f1.close()

'''

while True:
    try:
        x = int(input("Enter number:"))  # stack trace # throw exception -- catch -> handle the exception
        break
    except: # catch the granade
        # ERROR handling
        print("something went wrong int func int... please try again...")

print('need to be also here....')

#ZeroDivisionError
