def input_int(msg):
    while True:
        try:
            x = int(input(msg))
            return x
        except ValueError as err:
            print('-- bad number format. try again')