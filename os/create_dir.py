import os
#print(dir(os))

def create_nested(new_folder):
    # try to create all in once
    # if failed:
    # "inner/1610"
    # inner, 1610
    # create inner
    # 1610
    pass

print(os.getcwd())
print(os.listdir())
os.chdir('d:/itay')
print(os.listdir())
try:
    os.mkdir("inner2/1610")
except IOError as err:
    print('-------------------------------------------------------')
    print(f'--- Error: Failed to create folder {os.getcwd()}\\inner2\\1610')
    print('-------------------------------------------------------')

create_nested('c:/temp/hello/world')
print('done')