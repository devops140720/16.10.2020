import os
import logging
#print(dir(os))

import logging

def main():
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    logging.info('Started')
    print(1)
    logging.error('Finished')

main()

def create_nested(new_folder):
    # try to create all in once
    # if failed:
    # "inner/1610"
    # inner, 1610
    # create inner
    # 1610
    try:
        os.mkdir(new_folder)
        return
    except OSError:
        print('cannot create directly ... creating one step at a time')
    list_of_folders = new_folder.split('/')
    # inner/inner2/inner3
    # mkdir 'inner', mkdir 'inner/inner2', mkdir 'inner/inner2/inner3'
    # mkdri 'inner' -- chdir('inner') , mkdir 'inner2'
    # 'd:/itay/inner2/1610/123'
    for folder_name in list_of_folders:
        print(os.getcwd())
        d1 = os.getcwd()
        try:
            os.mkdir(folder_name)
        except:
            # folder already exist
            pass
        os.chdir(folder_name)

# inner2/1610/123
# inner2,1610,123
def recursvie_created(path1):
    list_of_folders = new_folder.split('/')
    if len(list_of_folders) == 0:
        return
    print(os.getcwd())
    folder_name = list_of_folders[0]
    try:
        os.mkdir(folder_name)
    except:
        # folder already exist
        pass
    os.chdir(folder_name)
    recursvie_created('/'.join(list_of_folders[1:])) # 1610/123

print(os.getcwd())
print(os.listdir())
os.chdir('d:/itay')
print(os.listdir())
try:
    os.mkdir("inner2/1610")
except IOError as err:
    print('-------------------------------------------------------')
    print(f'--- Error: Failed to create folder {os.getcwd()}\\inner2\\1610')
    print('-------------------------------------------------------')

create_nested('inner2/1610/123')
os.chdir('d:/itay')
recursvie_created('inner3/1610/400')
print('done')
